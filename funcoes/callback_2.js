/**
 * *****
 * File: callback_2.js
 * Project: funcoes
 * File Created: Friday, 26 February 2021 11:23:32
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Friday, 26 February 2021 16:30:45
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 * -----
 * Description:
 *
 * *****
 */

const fs = require('fs');
const path = require('path');

const caminho = path.join(__dirname, 'dados.txt'); // não é "__filename", rs

function exibirConteudo(err, conteudo) {
    console.log(conteudo.toString());
}

console.log('Inicio...');
// fs.readFile(caminho, {}, exibirConteudo);
fs.readFile(caminho, exibirConteudo); // também funciona assim, desde que a ordem dos parâmetros seja a mesma, e também os "obrigatórios" sejam respeitados.
fs.readFile(caminho, (_, conteudo) => console.log(conteudo.toString())); // "_" ignora o parâmetro dizendo que ele não é importante, quando possível.
console.log('...final.');

console.log('Inicio Sync...');
const conteudo = fs.readFileSync(caminho);
console.log(conteudo.toString());
console.log('...Final Sync.');
