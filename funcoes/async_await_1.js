/**
 * *****
 * File: async_await_1.js
 * Project: funcoes
 * File Created: Tuesday, 25 May 2021 18:31:21
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Wednesday, 26 May 2021 16:04:27
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 * -----
 * Description:
 *              Assincronismo e sincronismo.
 * *****
 */

function retornarValor() {
    return new Promise(resolve => {
        setTimeout(() => resolve(10), 5000);
    });
}

function esperarPor(tempo = 2000) {
    return new Promise(function(resolve) {
        setTimeout(() => resolve(), tempo);
    });
}

// esperarPor()
//     .then(() => console.log('Executando promise...'))
//     .then(esperarPor)
//     .then(() => console.log('Executando promise...'))
//     .then(esperarPor)
//     .then(() => console.log('Executando promise...'));

async function executar() {
    let valor = await retornarValor();
    await esperarPor(1500);
    console.log(`Async/Await ${valor}...`);
    await esperarPor(1500);
    console.log(`Async/Await ${valor + 1}...`);
    await esperarPor(1500);
    console.log(`Async/Await ${valor + 2}...`);

    return valor + 3;
}

// const v = executar(); // não funciona, pois retorna uma promise.
// console.log('v: ', v); // não funciona, pois retorna uma promise.
// const v = await executar(); // não funciona, pois o node não permite chamar diretamente o await em um módulo dele.

// executar().then(console.log); // funciona, pois aguarda o retorna da promise, e depois imprime na tela o resultado.

// outra forma de utilizar função assíncrona:
async function executarDeVerdade() {
    const valor = await executar();
    console.log('valor: ', valor);
}

executarDeVerdade(); // também funciona, pois a promise já foi resolvida.