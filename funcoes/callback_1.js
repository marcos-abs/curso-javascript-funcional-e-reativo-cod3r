/**
 * *****
 * File: callback_1.js
 * Project: funcoes
 * File Created: Wednesday, 24 February 2021 19:23:28
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Wednesday, 24 February 2021 19:38:03
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 * -----
 * Description:
 *              Desafio Rápido Callbacks:
 *      exec(somarNoTerminal, 56, 38)
 *      exec(subtrairNoTerminal, 182, 27)
 * *****
 */

const exec = (fn, a, b) => {
    console.log(fn(a, b));
};

const somarNoTerminal = (a, b) => a + b;
const subtrairNoTerminal = (a, b) => a - b;

exec(somarNoTerminal, 56, 38);
exec(subtrairNoTerminal, 182, 27);

setInterval(function () {
    console.log('Exec 2...');
}, 1000);

setInterval(() => {
    console.log('Exec 3...');
}, 1000);
