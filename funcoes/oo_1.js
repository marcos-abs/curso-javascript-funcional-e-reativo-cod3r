/**
 * *****
 * File: oo_1.js
 * Project: funcoes
 * File Created: Wednesday, 26 May 2021 16:32:57
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Wednesday, 26 May 2021 18:18:03
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 * -----
 * Description:
 *              Um pouco de orientação ao objeto.
 * *****
 */

function Produto(nome, preco, desc = 0.50) { // IMPORTANT função construtora.
    this.nome = nome;
    this.preco = preco;
    this.desc = desc;

    this.precoFinal = function () {
        return this.preco * (1 - this.desc);
    };
}

const p1 = new Produto('Caneta', 10);
console.log('p1.nome =', p1.nome);

const p2 = new Produto('Geladeira', 3000);
console.log('p2.preco =', p2.preco);
console.log('p2.precoFinal =', p2.precoFinal());
