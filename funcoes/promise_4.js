/**
 * *****
 * File: promise_4.js
 * Project: funcoes
 * File Created: Monday, 24 May 2021 20:19:36
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Monday, 24 May 2021 20:33:44
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 * -----
 * Description:
 *                  Outros exemplos de promises, agora com chamadas assíncronas e paralelismo.
 * *****
 */


function gerarNumerosEntre(min, max, tempo) {
    if (min > max) [max, min] = [min, max]; // destructuring "[max, min]" com array "[min, max]" (interessante). -- fonte: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment
    return new Promise(resolve => {
        setTimeout(function () {
            const fator = (max - min + 1);
            const aleatorio = parseInt(Math.random() * fator) + min;
            resolve(aleatorio);
        }, tempo);
    });
}

function gerarVariosNumeros() {
    return Promise.all([
        gerarNumerosEntre(1, 60, 4000),
        gerarNumerosEntre(1, 60, 1000),
        gerarNumerosEntre(1, 60, 500),
        gerarNumerosEntre(1, 60, 3000),
        gerarNumerosEntre(1, 60, 100),
        gerarNumerosEntre(1, 60, 1500)
    ]);
}
console.time('promise');
gerarVariosNumeros()
    .then(console.log)
    .then(() => {
        // console.timeLog('promise'); // neste caso, ficou desnecessário por conta do valor ser o mesmo do timer final.
        console.timeEnd('promise');
    });
