/**
 * *****
 * File: desafio_1.js
 * Project: funcoes
 * File Created: Wednesday, 24 February 2021 02:56:13
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Wednesday, 24 February 2021 03:09:08
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 * -----
 * Description:
 *              Criar funções:
 *                  1) somar(3)(4)(5)
 *                  2) fn -> 3 * 7
 *                     fn -> 3 + 7
 *                     fn -> 3 - 7
 *                     calcular(3)(7)(fn)
 * *****
 */

function somar(numero1) {
    return function (numero2) {
        return function (numero3) {
            return numero1 + numero2 + numero3;
        };
    };
}

// Desafio1
let resultado = somar(3);
resultado = resultado(4);
resultado = resultado(5);

console.log(resultado);

// Desafio2
function calcular(primeiro) {
    return function (segundo) {
        return function (fn) {
            if(typeof fn === 'function') { // "===" == comparação estrita
                return fn(primeiro, segundo);
            }
        };
    };
}

function funcaoMultiplicar(n1, n2) {
    return n1 * n2;
}

function funcaoSomar(n1, n2) {
    return n1 + n2;
}

function funcaoSubtrair(n1, n2) {
    return n1 - n2;
}

const resultado1 = calcular(3)(7)(funcaoMultiplicar);
const resultado2 = calcular(3)(7)(funcaoSomar);
const resultado3 = calcular(3)(7)(funcaoSubtrair);

console.log(resultado1);
console.log(resultado2);
console.log(resultado3);
