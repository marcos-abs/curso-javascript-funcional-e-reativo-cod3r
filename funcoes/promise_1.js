/**
 * *****
 * File: promise_1.js
 * Project: funcoes
 * File Created: Friday, 21 May 2021 12:08:23
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Monday, 24 May 2021 16:47:20
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 * -----
 * Description:
 *              Exemplos do uso de "promises".
 * *****
 */

const primeiroElemento = arrayOuString => arrayOuString[0]; // legal!
const letraMinuscula = letra => letra.toLowerCase();

new Promise(function (resolve) {
    resolve(['Ana', 'Bia', 'Carlos', 'Daniel']);
})
    .then(primeiroElemento)
    .then(primeiroElemento)
    .then(letraMinuscula)
    .then(v => console.log('v =', v));
