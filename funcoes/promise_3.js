/**
 * *****
 * File: promise_3.js
 * Project: funcoes
 * File Created: Monday, 24 May 2021 18:51:07
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Monday, 24 May 2021 19:07:03
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 * -----
 * Description:
 *              Outros exemplos de promises não assíncronos (síncronos).
 * *****
 */

function gerarNumerosEntre(min, max) {
    if (min > max) {
        [max, min] = [min, max]; // destructuring "[max, min]" com array "[min, max]" (interessante). -- fonte: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment
    }

    return new Promise(resolve => {
        const fator = (max - min + 1);
        const aleatorio = parseInt(Math.random() * fator) + min;
        resolve(aleatorio);
    });
}

gerarNumerosEntre(1, 60)
    .then(num => num * 10)
    .then(numX10 => `O número gerado foi ${numX10}`)
    .then(console.log);
