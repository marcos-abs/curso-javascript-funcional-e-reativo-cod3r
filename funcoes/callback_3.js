/**
 * *****
 * File: callback_3.js
 * Project: funcoes
 * File Created: Friday, 26 February 2021 16:38:31
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Friday, 26 February 2021 17:51:43
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 * -----
 * Description:
 *              Entendendo a função map (programação funcional)
 * *****
 */

const nums = [1, 2, 3, 4, 5];
// const dobro = n => n * 2; // n = elementos
// const dobro = (n, i) => n * 2 + i; // n = elementos, i = índice do Array
const dobro = (n, i, a) => n * 2 + i + a.length; // n = elementos, i = índice do Array, a = o próprio Array.
console.log(nums.map(dobro));

const nomes = ['Ana', 'Bia', 'Gui', 'Lia', 'Rafa'];
const primeiraLetra = texto => texto[0];
const letras = nomes.map(primeiraLetra);
console.log(nomes, letras);

const carrinho = [
    {nome: 'Caneta', qtde: 10, preco: 7.99 },
    {nome: 'Impressora', qtde: 0, preco: 649.50 },
    {nome: 'Caderno', qtde: 4, preco: 27.10 },
    {nome: 'Lapis', qtde: 3, preco: 5.82 },
    {nome: 'Tesoura', qtde: 1, preco: 19.20 }
];

const getNome = item => item.nome;
// console.log(carrinho.map(function(value) { // forma expandida
//     return getNome(value);
// }));
// console.log(carrinho.map(getNome)); // forma resumida.

// const getTotal = item => item.qtde * item.preco;
// const totais = carrinho.map(getTotal);
// console.log(totais);

// Construindo meu próprio "map":
Array.prototype.meuMap = function(fn) { // não pode ser "arrow function", pois é necessário o acesso ao "Array" através do "this", caso contrário não temos acesso (redundante mais didático).
    const mapped = [];
    for (let i = 0; i < this.length; i++) {
        // const result = fn(this[i], i, this);
        // mapped.push(`==> ${result}`);
        mapped.push(fn(this[i], i, this));
    }
    return mapped;
};

console.log(carrinho.meuMap(getNome)); // forma resumida.

const getTotal = item => item.qtde * item.preco;
const totais = carrinho.meuMap(getTotal);
console.log(totais);
