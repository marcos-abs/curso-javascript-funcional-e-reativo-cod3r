/**
 * *****
 * File: basico_3.js
 * Project: funcoes
 * File Created: Wednesday, 24 February 2021 12:49:07
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Wednesday, 24 February 2021 14:28:22
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 * -----
 * Description:
 *              Arrow function
 * *****
 */

const felizNatal = () => console.log('Feliz Natal!!');
felizNatal();

const saudacao = (nome) => "Fala " + nome + "!!"; // neste caso não tem "return"
console.log(saudacao('Maria'));

const saudacao2 = (nome) => {
    return "Fala " + nome + "blz?!"; // neste caso sim tem "return"
};
console.log(saudacao2('José'));

const saudacao3 = nome => { // assim, sem "()", também funciona!!
    return "Fala " + nome + "blz?!";
};
console.log(saudacao3('Antônio'));

const saudacao4 = nome => `Fala ${nome} blz?!`; // só funciona com crases "`".
console.log(saudacao4('João'));

const resultado = numero => `O resultado é ${ 2 * 2 }.`; // operações aritméticas também funcionam.
console.log(resultado()); // só funciona com os "()" parenteses.

const somar = numeros => {
    let total = 0;
    for(let n of numeros) {
        total += n;
    }
    return total;
};

console.log(somar([
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10
]));

const somar2 = (...numeros) => { // Spread operator "..." https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_syntax
    // console.log(typeof numeros); === object
    // console.log(Array.isArray(numeros)); === array
    if(Array.isArray(numeros)) { // gostei!!
        let total = 0;
        for(let n of numeros) {
            total += n;
        }
        return total;
    }
};

console.log(somar2(
    2, 4, 6, 8, 10, 12, 14, 16, 18, 20
));

const subtrair = (a, b) => a - b;
console.log(subtrair(4, 1));

// micro desafio arrow functions

const potencia = base => {              // escrita simples
    return exp => {
        return Math.pow(base, exp);
    };
};

console.log(potencia(2)(8));

const potencia2 = base =>               // melhorando a escrita
    exp => {
        return Math.pow(base, exp);
    };


console.log(potencia2(2)(10));

const potencia3 = base => exp => Math.pow(base, exp); // melhorando ainda mais a escrita
console.log(potencia3(3)(8));


// this
Array.prototype.log = function() {
    console.log(this);
};

const numeros = [1, 2, 3];
numeros.log();

Array.prototype.ultimo = function() {
    console.log(this[this.length - 1]); // interessante.
};

const numeros2 = [1, 2, 3];
numeros2.ultimo();

Array.prototype.primeiro = function() {
    console.log(this[0]); // interessante.
};

const numeros3 = [1, 2, 3];
numeros3.primeiro();
