/**
 * *****
 * File: basico_2.js
 * Project: funcoes
 * File Created: Wednesday, 24 February 2021 02:36:36
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Wednesday, 24 February 2021 02:54:55
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 * -----
 * Description:
 *              Mecânica programação function e programação reativa.
 * *****
 */

function bomDia() {
    console.log('Bom dia!');
}

const boaTarde = function () { // função anônima ou função sem nome
    console.log('Boa tarde!');
}

function executarQualquerCoisa(fn) {
    // console.log(typeof fn);
    if(typeof fn === 'function') { // "===" == comparação estrita
        fn();
    }
}

executarQualquerCoisa(3);
executarQualquerCoisa(bomDia);
executarQualquerCoisa(boaTarde);

function potencia(base) {
    return function (exp) {
        return Math.pow(base, exp);
    }
}

const potenciaDe2 = potencia(2);
console.log(potenciaDe2(8)); // gostei.

const pot34 = potencia(3)(4); // outra forma de uso
console.log(pot34);
