/**
 * *****
 * File: async_await_2.js
 * Project: funcoes
 * File Created: Wednesday, 26 May 2021 16:06:51
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Wednesday, 26 May 2021 16:29:44
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 * -----
 * Description:
 *              Mais um exemplo de utilização de promise com async e await.
 * *****
 */

function gerarNumerosEntre(min, max, numerosProibidos) {
    if (min > max) {
        [max, min] = [min, max]; // destructuring "[max, min]" com array "[min, max]" (interessante). -- fonte: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment
    }

    return new Promise((resolve, reject) => {
        const fator = (max - min + 1);
        const aleatorio = parseInt(Math.random() * fator) + min;
        if (numerosProibidos.includes(aleatorio)) {
            reject('Número repetido');
        } else {
            resolve(aleatorio);
        }
    });
}

// gerarNumerosEntre(1, 5, [1, 2, 4])
//     .then(console.log)
//     .catch(console.log);

async function gerarMegaSena(qtdeNumeros, tentativas = 1) { //IMPORTANTE: função assíncrona "sempre" retorna uma "promise".
    try {
        const numeros = [];
        for (let _ of Array(qtdeNumeros).fill()) {
            numeros.push(await gerarNumerosEntre(1, 60, numeros));
        }
        return numeros;
    } catch (e) {
        if (tentativas > 10) {
            throw "Que chato!!! não deu certo...";
        }
        return gerarMegaSena(qtdeNumeros, tentativas + 1);
    }
}

gerarMegaSena(15)
    .then(console.log)
    .catch(console.log);
