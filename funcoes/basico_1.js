/**
 * *****
 * File: basico_1.js
 * Project: funcoes
 * File Created: Wednesday, 24 February 2021 01:36:03
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Wednesday, 24 February 2021 02:36:27
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 * -----
 * Description:
 *              Configurações iniciais.
 * *****
 */

// Function Declaration
function bomDia() {
    console.log('Bom dia!');
}
bomDia();

// Function Expression
const boaTarde = function () {
    console.log('Boa tarde!');
};

boaTarde();

function somar(a = 0, b = 0) { // os parâmetros "a" e "b" não são obrigatórios.
    return a + b;
}

let resultado = somar(3, 4);
console.log(resultado);

resultado = somar(3, 5, 7, 9, 11);
console.log(resultado);

resultado = somar(3);
console.log(resultado);

resultado = somar();
console.log(resultado);
