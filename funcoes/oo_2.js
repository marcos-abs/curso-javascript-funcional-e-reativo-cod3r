/**
 * *****
 * File: oo_2.js
 * Project: funcoes
 * File Created: Wednesday, 26 May 2021 18:14:20
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Wednesday, 26 May 2021 18:49:32
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 * -----
 * Description:
 *              Mais Um pouco de orientação ao objeto.
 * *****
 */

class Produto {
    constructor (nome, preco, desc = 0.5) {
        this.nome = nome;
        this.preco = preco;
        this.desc = desc;
    }

    get nome() {
        return `Produto: ${this._nome}`; //IMPORTANT não é mais um atributo e sim um método.
    }

    set nome(novoNome) {
        this._nome = novoNome.toUpperCase();
    }

    get preco() {
        return this._preco;
    }

    set preco(novoPreco) {
        if (novoPreco >= 0) {
            this._preco = novoPreco;
        }
    }

    get precoFinal() {
        return this.preco * (1 - this.desc);
    }
}

const p1 = new Produto('Caneta', 10);
// p1.nome = 'caneta';
console.log('p1.nome =', p1.nome);

const p2 = new Produto('Geladeira', 10000, 0.8);
console.log('p2.preco =', p2.preco);
console.log('p2.precoFinal =', p2.precoFinal); //IMPORTANT "p2.precoFinal" não é mais chamado como função e sim como atributo da classe.
