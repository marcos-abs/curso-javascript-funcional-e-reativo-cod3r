/**
 * *****
 * File: promise_2.js
 * Project: funcoes
 * File Created: Monday, 24 May 2021 16:48:49
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Monday, 24 May 2021 18:49:47
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 * -----
 * Description:
 *              Comparativo do uso de Promise e do uso de Callback.
 * *****
 */

// setTimeout(function () {
//     console.log('Executando CallBack1...');
//     setTimeout(function () {
//         console.log('Executando CallBack2...');
//         setTimeout(function () {
//             console.log('Executando CallBack3...');
//         }, 2000);
//     }, 2000);
// }, 2000);

function esperarPor(tempo = 2000) {
    return new Promise(function(resolve) {
        setTimeout(function() {
            console.log('Executando Promise...');
            resolve();
        }, tempo);
    });
}

esperarPor()
    .then(() => esperarPor())
    .then(esperarPor); // funcionou agora!
