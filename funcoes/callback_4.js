/**
 * *****
 * File: callback_4.js
 * Project: funcoes
 * File Created: Thursday, 20 May 2021 13:14:12
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Thursday, 20 May 2021 13:45:14
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 * -----
 * Description:
 *              Exemplos de Filter, Map e Prototype.
 * *****
 */
const carrinho = [
    {nome: 'Caneta', qtde: 10, preco: 7.99 },
    {nome: 'Impressora', qtde: 0, preco: 649.50 },
    {nome: 'Caderno', qtde: 4, preco: 27.10 },
    {nome: 'Lapis', qtde: 3, preco: 5.82 },
    {nome: 'Tesoura', qtde: 1, preco: 19.20 }
];

const qtdeMaiorQueZero = item => item.qtde > 0;
const qtdeMuitoGrande = item => item.qtde >= 1000;
const qtdeMaiorIgualZero = item => item.qtde >= 0;
const getNome = item => item.nome;

const itensValidos = carrinho.filter(qtdeMaiorQueZero);

console.log('itensValidos: ', itensValidos);

const nomeItensValidos = carrinho
    .filter(qtdeMaiorQueZero)
    .map(getNome);

console.log('\n nomeItensValidos: ', nomeItensValidos);

const nomeItensValidos2 = carrinho
    .filter(qtdeMuitoGrande)
    .map(getNome);

console.log('\n nomeItensValidos2: ', nomeItensValidos2);

const nomeItensValidos3 = carrinho
    .filter(qtdeMaiorIgualZero)
    .map(getNome);

console.log('\n nomeItensValidos3: ', nomeItensValidos3);

Array.prototype.meuFilter = function (fn) {
    const novoArray = [];
    for (let i = 0; i < this.length; i++) {
        if (fn(this[i], i, this)) {
            novoArray.push(this[i]);
        }
    }
    return novoArray;
};

const nomeItensValidos4 = carrinho
    .meuFilter(qtdeMaiorQueZero)
    .map(getNome);

console.log('\n nomeItensValidos4: ', nomeItensValidos4);