/**
 * *****
 * File: callback_5a.js
 * Project: funcoes
 * File Created: Friday, 21 May 2021 11:50:57
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Friday, 21 May 2021 12:02:08
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 * -----
 * Description:
 *              Reduce - continuação.
 * *****
 */
const carrinho = [
    {nome: 'Caneta', qtde: 10, preco: 7.99 },
    {nome: 'Impressora', qtde: 0, preco: 649.50 },
    {nome: 'Caderno', qtde: 4, preco: 27.10 },
    {nome: 'Lapis', qtde: 3, preco: 5.82 },
    {nome: 'Tesoura', qtde: 1, preco: 19.20 }
];

const getTotal = item => item.qtde * item.preco;
const somar = (acc, el) => acc + el;

const totalGeral = carrinho
    .map(getTotal)
    .reduce(somar);

console.log('totalGeral: ', totalGeral);

Array.prototype.meuReduce = function (fn, inicial) {
    let acc = inicial;
    for (let i = 0; i < this.length; i++) {
        if (!acc && i === 0) {
            acc = this[i];
            continue;
        }
        acc = fn (acc, this[i], i, this); // argumentos do reduce normal utilizados aqui para representar o que o comando (reduce) faz.
    }
    return acc;
};

const totalGeral2 = carrinho
    .map(getTotal)
    .meuReduce(somar);

console.log('\ntotalGeral2: ', totalGeral2);
