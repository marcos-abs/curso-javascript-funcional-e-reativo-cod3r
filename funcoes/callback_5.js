/**
 * *****
 * File: callback_5.js
 * Project: funcoes
 * File Created: Thursday, 20 May 2021 15:40:26
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Thursday, 20 May 2021 16:01:29
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 * -----
 * Description:
 *              Exemplos com Reduce.
 * *****
 */
const carrinho = [
    {nome: 'Caneta', qtde: 10, preco: 7.99 },
    {nome: 'Impressora', qtde: 0, preco: 649.50 },
    {nome: 'Caderno', qtde: 4, preco: 27.10 },
    {nome: 'Lapis', qtde: 3, preco: 5.82 },
    {nome: 'Tesoura', qtde: 1, preco: 19.20 }
];

const getTotal = item => item.qtde * item.preco;

const totalGeral = carrinho.map(getTotal);

console.log('totalGeral: ', totalGeral);

const somar = (acc, el) => { // acc = acumulador/valor anterior e el = elemento a ser processado.
    console.log('acc, el, resultado: ', acc, el, acc + el);
    return acc + el;
};

const totalGeral2 = carrinho
    .map(getTotal)
    .reduce(somar);

console.log('\n totalGeral2: ', totalGeral2);

const totais = carrinho.map(getTotal);
console.log('\ntotais: ', totais);

const totalGeral3 = totais.reduce(somar);
console.log('\ntotalGeral3: ', totalGeral3);

const totalGeral4 = carrinho
    .map(getTotal)
    .reduce(somar);

console.log('\ntotalGeral4: ', totalGeral4);