/**
 * *****
 * File: desafio_3.js
 * Project: funcoes
 * File Created: Monday, 24 May 2021 19:10:00
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Monday, 24 May 2021 20:13:53
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 * -----
 * Description:
 *              Desafio número 3 - Criar uma promise que leia o conteudo de um arquivo.
 * *****
 */
const fs = require('fs');
const path = require('path');

function exibirConteudo(err, conteudo) {
    console.log(conteudo.toString());
}

// fs.readFile(caminho, (_, conteudo) => console.log(conteudo.toString())); // "_" ignora o parâmetro dizendo que ele não é importante, quando possível.

function lerArquivo(caminho) {
    return new Promise(resolve => {
        fs.readFile(caminho, function (_, conteudo) {  // "_" ignora o parâmetro dizendo que ele não é importante, quando possível.
            resolve(conteudo.toString());
        });
    });
}

const caminho = path.join(__dirname, 'dados.txt');

lerArquivo(caminho)
    .then(conteudo => conteudo.split('\n'))
    .then(linhas => linhas.join(','))
    .then(conteudo => `O conteudo do arquivo é: ${conteudo}`)
    .then(console.log);
