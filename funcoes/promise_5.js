/**
 * *****
 * File: promise_5.js
 * Project: funcoes
 * File Created: Tuesday, 25 May 2021 17:59:55
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Tuesday, 25 May 2021 18:29:47
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 * -----
 * Description:
 *              Promises com resolve e reject.
 * *****
 */
function funcionarOuNao(valor, chanceErro) {
    return new Promise((resolve, reject) => {
        try {
            con.log('temp');
            if (Math.random() < chanceErro) {
                reject('Ocorreu um erro.');
            } else {
                resolve(valor);
            }
        } catch (e) {
            resolve(e);
        }
    });
}

funcionarOuNao('Testando...', 0.5)
    .then(v => `Valor: ${v}`)
    .then(
        v => console.log(v),
        // err => console.log(`Erro Esp.: ${err}`)
    )
    .then(() => console.log('Quase Fim!'))
    .catch(err => console.log(`Erro Geral: ${err}`)) // dica: normalmente colocar o "catch" no final e não no "meio" dos "then".
    .then(() => console.log('Fim!'));
