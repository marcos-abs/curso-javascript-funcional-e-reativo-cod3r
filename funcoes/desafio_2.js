/**
 * *****
 * File: desafio_2.js
 * Project: funcoes
 * File Created: Friday, 21 May 2021 00:00:02
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Friday, 21 May 2021 00:51:48
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 * -----
 * Description:
 *              Desafio número 2: "filter, map e reduce"
 *              1) filtrar somente os itens com o campo "fragil" == true;
 *              2) totalizar cada um dos produtos (qtde: x preco: = total);
 *              3) calcular a média dos totais dos produtos (dentro da função).
 * *****
 */

const carrinho = [
    {nome: 'Caneta', qtde: 10, preco: 7.99, fragil: true },
    {nome: 'Impressora', qtde: 1, preco: 649.50, fragil: true },
    {nome: 'Caderno', qtde: 4, preco: 27.10, fragil: false },
    {nome: 'Lapis', qtde: 3, preco: 5.82, fragil: false },
    {nome: 'Tesoura', qtde: 1, preco: 19.2, fragil: true }
];

// item 1:
// const fragilIgualTrue = item => item.fragil === true;
// const itensValidos = carrinho.filter(fragilIgualTrue);

// item 2:
// const getTotal = item => item.qtde * item.preco;
// const totalGeral = carrinho.map(getTotal);
// console.log('item 2: ', totalGeral);

// item 1, 2, 3:
const isFragil = item => item.fragil;
const getTotal = item => item.qtde * item.preco;
const getMedia = (acc, el) => {
    const novaQtde = acc.qtde + 1;
    const novoTotal = acc.total + el;
    console.log('acc, el: ', acc, el);
    return {
        qtde: novaQtde,
        total: novoTotal,
        media: novoTotal / novaQtde
    };
};

const mediaInicial = { qtde: 0, total: 0, media: 0 };

const media = carrinho
    .filter(isFragil)
    .map(getTotal)
    .reduce(getMedia, mediaInicial)
    .media;

console.log('A média é: ', media, '!');