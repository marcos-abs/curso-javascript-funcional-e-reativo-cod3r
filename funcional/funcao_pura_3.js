/**
 * *****
 * File: funcao_pura_3.js
 * Project: funcional
 * File Created: Wednesday, 02 June 2021 01:28:52
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Wednesday, 02 June 2021 01:38:23
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 * -----
 * Description:
 *              Uma função pura é uma função em que o valor de retorno é
 *  determinado "APENAS" por seus valores de entrada, sem efeitos colaterais
 *  observáveis.
 *
 * *****
 */
let qtdeDeExecucoes = 0;

// essa "era" uma função pura! simples e de resultado previsível, porém com o acréscimo da variável "qtdeDeExecucoes", transformou-a em impura.
function somar(a, b) {
    qtdeDeExecucoes++; // efeitos colaterais observáveis.
    return a + b;
}
console.log('qtdeDeExecucoes: ', qtdeDeExecucoes);
console.log('somar(68, 31): ', somar(68, 31));
console.log('somar(68, 31): ', somar(68, 31));
console.log('somar(68, 31): ', somar(68, 31));
console.log('somar(68, 31): ', somar(68, 31));
console.log('somar(68, 31): ', somar(68, 31));
console.log('qtdeDeExecucoes: ', qtdeDeExecucoes);
