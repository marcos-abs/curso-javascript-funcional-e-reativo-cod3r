/**
 * *****
 * File: composicao_2.js
 * Project: funcional
 * File Created: Monday, 07 June 2021 10:55:54
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Monday, 07 June 2021 11:05:47
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 * -----
 * Description:
 *
 * *****
 */

function composicao(...fns) {
    return function (valor) {
        return fns.reduce(async (acc, fn) => {
            if (Promise.resolve(acc) === acc) { // é uma "promise".
                return fn(await acc);
            } else {
                return fn(acc);
            }
        }, valor);
    };
}

function gritar(texto) {
    return texto.toUpperCase();
}

function enfatizar(texto) {
    return `${texto}!!!!`;
}

function tornarLento(texto) {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve(texto.split('').join(' '));
        }, 3000); // 3 segundos.
    });
}

const exagerado = composicao(
    gritar,
    enfatizar,
    tornarLento
);

const umPoucoMenosExagerado = composicao(
    gritar,
    enfatizar
);

exagerado('pare')
    .then(console.log);
umPoucoMenosExagerado('cuidado com o buraco')
    .then(console.log);
