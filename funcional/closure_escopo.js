/**
 * *****
 * File: closure_escopo.js
 * Project: funcional
 * File Created: Wednesday, 02 June 2021 21:40:12
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Wednesday, 02 June 2021 21:48:49
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 * -----
 * Description:
 *
 * *****
 */

const x = 3;

function fora() {
    const x = 97;
    function somarXMais3() {
        return x + 3;
    }
    return somarXMais3;
}

module.exports = fora();