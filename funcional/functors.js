/**
 * *****
 * File: functors.js
 * Project: funcional
 * File Created: Monday, 07 June 2021 18:35:26
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Monday, 07 June 2021 18:55:35
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 * -----
 * Description: Wrapper - Container de functions.
 *      Functors são objetos que implementam a função MAP que também é um
 * "wrapper" de um valor.
 * *****
 */

// ARRAY é um exemplo de FUNCTORS
const nums = [1, 2, 3, 4, 5, 6];
const novosNums = nums
    .map(el => el + 10)
    .map(el => el * 2);

console.log('nums: ', nums);
console.log('novosNums: ', novosNums);

function TipoSeguro(valor) {
    return {
        valor,
        invalido() {
            return this.valor === null || this.valor === undefined;
        },
        map(fn) {
            if (this.invalido()) {
                return TipoSeguro(null);
            } else {
                const novoValor = fn(this.valor);
                return TipoSeguro(novoValor);
            }
        }
    };
}

const resultado = TipoSeguro('Esse é um texto')
    .map(t => t.toUpperCase())
    .map(t => null) //.map(t => `${t}!!!!`)
    .map(t => t.split('').join(' '));

console.log('resultado.valor: ', resultado.valor);
