/**
 * *****
 * File: currying_1.js
 * Project: funcional
 * File Created: Friday, 04 June 2021 12:46:28
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Friday, 04 June 2021 14:12:23
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 * -----
 * Description:
 *
 * *****
 */

function textoComTamanhoEntre(min, max, erro, texto) {
    const tamanho = (texto || '').trim().length;
    if (tamanho < min || tamanho > max) {
        throw erro;
    }
}

const p1 = { nome: 'A', preco: 14.99, desc: 0.25 };

textoComTamanhoEntre(4, 255, 'Nome inválido!', p1.nome);
