/**
 * *****
 * File: higher_order_function.js
 * Project: funcional
 * File Created: Wednesday, 02 June 2021 01:42:25
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Wednesday, 02 June 2021 01:53:55
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 * -----
 * Description:
 *              Funções que operam em outras funções, tomando-as como
 *  argumentos ou retornando-as, são chamadas de "higher-order function".
 *
 * *****
 */

function executar(fn, ...params) {
    return function (textoInicial) {
        return `${textoInicial} ${fn(...params)}!`;
    };
}

function somar(a, b, c) {
    return a + b + c;
}

function multi(a, b) {
    return a * b;
}

const r1 = executar(somar, 4, 5, 6)('O resultado da soma é ');
const r2 = executar(multi, 30, 40)('O resultado da multiplicação é ');

console.log('r1: ', r1);
console.log('r2: ', r2);
