/**
 * *****
 * File: imutabilidade_3.js
 * Project: funcional
 * File Created: Wednesday, 02 June 2021 15:15:26
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Wednesday, 02 June 2021 21:36:42
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 * -----
 * Description:
 *
 * *****
 */

const pessoa = Object.freeze({
    nome: 'Maria',
    altura: 1.76,
    cidade: 'São Paulo',
    end: Object.freeze({
        rua: 'Feliz!'
    })
});

// Atribuição por Referência.
const outraPessoa = pessoa;

// Passagem por Referência (Função impura!)
function alteraPessoa(pessoa) {
    const novaPessoa = { ...pessoa };
    novaPessoa.nome = 'João';
    novaPessoa.cidade = 'Fortaleza';
    novaPessoa.end.rua = 'ABC';
    return novaPessoa;
}

const novaPessoa = alteraPessoa(pessoa);

console.log('pessoa: ', pessoa);
console.log('novaPessoa: ', novaPessoa);
