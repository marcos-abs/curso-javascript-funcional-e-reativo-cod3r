/**
 * *****
 * File: imutabilidade_2.js
 * Project: funcional
 * File Created: Wednesday, 02 June 2021 15:00:17
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Wednesday, 02 June 2021 15:10:55
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 * -----
 * Description:
 *
 * *****
 */

const nums = [4, 8, 3, 2, 9, 1, 9, 3];

// #3 - Recursividade
function somarArray(array, total = 0) {
    if (array.length === 0) {
        return total;
    }
    return somarArray(array.slice(1), total + array[0]);
}
const total = somarArray(nums);
console.log('total: ', total);

// #2 - Reduce
// const somar = (a, b) => a + b;
// const total = nums.reduce(somar);
// console.log('total: ', total);

// #1 Dados mutáveis
// let total = 0;
// for (let i = 0; i < nums.length; i++) {
//     total += nums[i];
// }
// console.log('total: ', total);
