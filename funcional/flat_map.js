/**
 * *****
 * File: flat_map.js
 * Project: funcional
 * File Created: Monday, 07 June 2021 19:01:07
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Monday, 07 June 2021 19:38:25
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 * -----
 * Description:
 *
 * *****
 */

const letrasAninhadas = [ ['O', 'l', 'á' ], [' '], ['M', 'u', 'n', 'd', 'o' ], [' ', '!', '!', '!', '!'] ];
const letras = letrasAninhadas.flat(Infinity);
console.log(letras);

// const resultado = letras
//     .map(l => l.toUpperCase())
//     .reduce((a, b) => a + b);
// console.log('resultado: ', resultado);

