/**
 * *****
 * File: lazy_eval.js
 * Project: funcional
 * File Created: Monday, 07 June 2021 09:59:26
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Monday, 07 June 2021 10:11:23
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 * -----
 * Description:
 *              Lazy Evaluation - Avaliação atrasada
 * *****
 */

function eager(a, b) { // eager = apressado.

    // processamento "mais" pesado.
    const fim = Date.now() + 2500;
    while (Date.now() < fim) { }

    const valor = Math.pow(a, 3);
    return valor + b;
}

function lazy(a) { // lazy = atrasado.

    // processamento "mais" pesado.
    const fim = Date.now() + 2500;
    while (Date.now() < fim) { }

    const valor = Math.pow(a, 3);
    return function (b) {
        return valor + b;
    };
}

console.time('#1');
console.log(eager(3, 100));
console.log(eager(3, 200));
console.log(eager(3, 300));
console.timeEnd('#1');

console.time('#2');
const lazy3 = lazy(3);
console.log(lazy3(100));
console.log(lazy3(200));
console.log(lazy3(300));
console.timeEnd('#2');
