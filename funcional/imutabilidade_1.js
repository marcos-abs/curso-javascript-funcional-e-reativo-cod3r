/**
 * *****
 * File: imutabilidade_1.js
 * Project: funcional
 * File Created: Wednesday, 02 June 2021 02:08:10
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Wednesday, 02 June 2021 02:26:19
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 * -----
 * Description:
 *
 * *****
 */

function ordenar(array) {
    return [...array].sort((a, b) => a - b);
}

const nums = Object.freeze([3, 1, 7, 9, 4, 6]);
const numsOrdenados = ordenar(nums);
console.log('nums e numsOrdenados: ', nums, numsOrdenados);

const parteNums = nums.slice(2);
console.log('parteNums e nums: ', parteNums, nums);
