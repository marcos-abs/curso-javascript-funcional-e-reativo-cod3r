/**
 * *****
 * File: closure.js
 * Project: funcional
 * File Created: Wednesday, 02 June 2021 21:39:55
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Thursday, 03 June 2021 23:19:00
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 * -----
 * Description:
 *              Closure é quando uma função "lembra" seu escopo léxico, mesmo
 *   quando a função é executada fora desse escopo léxico.
 * *****
 */

const somarXMais3 = require('./closure_escopo');

const x = 1000;

console.log('somarXMais3: ', somarXMais3());

