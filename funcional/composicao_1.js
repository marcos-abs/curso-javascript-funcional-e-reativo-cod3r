/**
 * *****
 * File: composicao_1.js
 * Project: funcional
 * File Created: Monday, 07 June 2021 10:14:37
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Monday, 07 June 2021 10:49:26
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 * -----
 * Description:
 *
 * *****
 */

function composicao(...fns) {
    return function (valor) {
        return fns.reduce((acc, fn) => {
            return fn(acc);
        }, valor);
    };
}

function gritar(texto) {
    return texto.toUpperCase();
}

function enfatizar(texto) {
    return `${texto}!!!!`;
}

function tornarLento(texto) {
    return texto.split('').join(' ');
}

const exagerado = composicao(
    gritar,
    enfatizar,
    tornarLento
);

const umPoucoMenosExagerado = composicao(
    gritar,
    enfatizar
);

const resultado1 = exagerado('pare');
const resultado2 = umPoucoMenosExagerado('cuidado com o buraco');
console.log('resultado1: ', resultado1);
console.log('resultado2: ', resultado2);
