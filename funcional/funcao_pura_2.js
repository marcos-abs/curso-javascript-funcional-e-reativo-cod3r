/**
 * *****
 * File: funcao_pura_2.js
 * Project: funcional
 * File Created: Wednesday, 02 June 2021 01:15:36
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Wednesday, 02 June 2021 01:26:08
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 * -----
 * Description:
 *              Uma função pura é uma função em que o valor de retorno é
 *  determinado "APENAS" por seus valores de entrada, sem efeitos colaterais
 *  observáveis.
 *
 * *****
 */

function gerarNumeroAleatorio(min, max) {
    const fator = max - min + 1;
    return parseInt(Math.random() * fator) + min; // função impura - recebe um valor externo randômico.
}

console.log('gerarNumeroAleatorio(1, 10000): ', gerarNumeroAleatorio(1, 10000));
console.log('gerarNumeroAleatorio(1, 10000): ', gerarNumeroAleatorio(1, 10000));
console.log('gerarNumeroAleatorio(1, 10000): ', gerarNumeroAleatorio(1, 10000));
console.log('gerarNumeroAleatorio(1, 10000): ', gerarNumeroAleatorio(1, 10000));
console.log('gerarNumeroAleatorio(1, 10000): ', gerarNumeroAleatorio(1, 10000));
