/**
 * *****
 * File: funcao_pura_1.js
 * Project: funcional
 * File Created: Tuesday, 01 June 2021 15:11:28
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Wednesday, 02 June 2021 01:23:23
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 * -----
 * Description:
 *              Uma função pura é uma função em que o valor de retorno é
 *  determinado "APENAS" por seus valores de entrada, sem efeitos colaterais
 *  observáveis.
 *
 * *****
 */

// exemplo de função impura:
function areaCirc(raio) {
    return raio * raio * Math.PI; // "Math.PI" é um elemento externo, portanto suscetível a alterações no resultado da função.
}
console.log('areaCirc(10): ', areaCirc(10));
console.log('areaCirc(10): ', areaCirc(10));
console.log('areaCirc(10): ', areaCirc(10));

// exemplo de função pura:
function areaCircPura(raio, pi) {
    return raio * raio * pi;
}
console.log('areaCircPura(10, 3.14): ', areaCircPura(10, 3.14));
console.log('areaCircPura(10, 3.141592): ', areaCircPura(10, 3.141592));
console.log('areaCircPura(10, Math.PI): ', areaCircPura(10, Math.PI));