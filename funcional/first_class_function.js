/**
 * *****
 * File: first_class_function.js
 * Project: funcional
 * File Created: Wednesday, 02 June 2021 01:56:18
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Wednesday, 02 June 2021 02:04:58
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 * -----
 * Description:
 *              Diz-se que uma linguagem de programação possui funções
 *      de primeira classe quando funções nessa linguagem são tratadas
 *      como qualquer outra variável.
 *
 *      "first class citizen" - "cidadão de primeira classe".
 *
 * *****
 */

const x = 3;
const y = function (txt) {
    return `Esse é o texto: ${txt}`;
};

const z = () => console.log('Zzzzzzzz!');

console.log('x: ', x);
console.log('y: ', y('Olá'));
z();